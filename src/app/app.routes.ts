import { Routes } from '@angular/router';
import { HomeComponent } from './ui/home/home.component';

export const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('./ui/ui.module').then(module => module.UiModule)
  },
  {
    path: 'home',
    component: HomeComponent
  },
  {path: '', redirectTo: '/home', pathMatch: 'full'}
];
