import { Injectable } from "@angular/core";
import { CrudAdminWorker } from "../../../domain/interfaces/worker/crud.admin.worker";
import { Observable } from "rxjs";
import { AdminWorker } from "../../../domain/entitys/worker/admin.worker";

@Injectable()
export class AdminWorkerService implements CrudAdminWorker {
  create(admin: AdminWorker): Observable<AdminWorker> {
    throw new Error("Method not implemented.");
  }

  read(): Observable<AdminWorker[]> {
    throw new Error("Method not implemented.");
  }

  update(admin: AdminWorker, id: string): Observable<AdminWorker> {
    throw new Error("Method not implemented.");
  }

  delete(id: string): void {
    throw new Error("Method not implemented.");
  }

  showId(id: string): Observable<AdminWorker> {
    throw new Error("Method not implemented.");
  }
}
