import { Injectable } from "@angular/core";
import { CrudWorker } from "../../../domain/interfaces/worker/crud.worker";
import { Observable } from "rxjs";
import { Worker } from "../../../domain/entitys/worker/worker";

@Injectable()
export class WorkerService implements CrudWorker {
  create(worker: Worker): Observable<Worker> {
    throw new Error("Method not implemented.");
  }

  read(): Observable<Worker[]> {
    throw new Error("Method not implemented.");
  }

  update(worker: Worker, id: string): Observable<Worker> {
    throw new Error("Method not implemented.");
  }

  delete(id: string): void {
    throw new Error("Method not implemented.");
  }

  showId(id: string): Observable<Worker> {
    throw new Error("Method not implemented.");
  }
}
