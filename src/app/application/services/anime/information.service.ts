import { Injectable } from "@angular/core";
import { CrudInformation } from "../../../domain/interfaces/anime/crud.information";
import { Observable } from "rxjs";
import { Information } from "../../../domain/entitys/anime/information";

@Injectable()
export class InformationService implements CrudInformation {
  create(information: Information): Observable<Information> {
    throw new Error("Method not implemented.");
  }

  read(): Observable<Information[]> {
    throw new Error("Method not implemented.");
  }

  update(information: Information, id: string): Observable<Information> {
    throw new Error("Method not implemented.");
  }

  delete(id: string): void {
    throw new Error("Method not implemented.");
  }

  showId(id: string): Observable<Information> {
    throw new Error("Method not implemented.");
  }
}
