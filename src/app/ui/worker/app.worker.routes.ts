import { Routes } from '@angular/router';
import { HomeWorkerComponent } from './home/home.worker.component';
import { SignUpComponent } from './sign_up/signup.component';
import { AccountComponent } from './account/account.component';

export const routes: Routes = [
  {
    path: 'home-worker',
    component: HomeWorkerComponent,
    children: [
      {
        path: 'sign-up',
        component: SignUpComponent
      },
      {
        path: 'account',
        component: AccountComponent
      }
    ]
  },
];
