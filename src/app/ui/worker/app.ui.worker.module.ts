import { NgModule } from "@angular/core";
import { HomeWorkerComponent } from "./home/home.worker.component";
import { RouterLink, RouterOutlet, provideRouter } from "@angular/router";
import { routes } from "./app.worker.routes";
import { SignUpComponent } from "./sign_up/signup.component";
import { AccountComponent } from "./account/account.component";

@NgModule({
  declarations: [
    HomeWorkerComponent,
    SignUpComponent,
    AccountComponent
  ],
  imports: [
    RouterLink,
    RouterOutlet
  ],
  exports: [
    HomeWorkerComponent,
    SignUpComponent,
    AccountComponent
  ],
  providers: [provideRouter(routes)]
})
export class ApplicationUiWorkerModule { }
