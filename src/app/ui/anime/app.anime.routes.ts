import { Routes } from '@angular/router';
import { HomeAnimeComponent } from './home/home.anime.component';

export const routes: Routes = [
  {
    path: 'home-anime',
    component: HomeAnimeComponent,
  }
];

