import { NgModule } from "@angular/core";
import { HomeAnimeComponent } from "./home/home.anime.component";
import { provideRouter } from "@angular/router";
import { routes } from "./app.anime.routes";

@NgModule({
  declarations:[
    HomeAnimeComponent
  ],
  exports: [
    HomeAnimeComponent
  ],
  providers: [provideRouter(routes)]
})
export class ApplicationUiAnimeModule { }
