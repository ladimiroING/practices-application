import { NgModule } from "@angular/core";
import { ApplicationUiAnimeModule } from "./anime/app.ui.anime.module";
import { ApplicationUiWorkerModule } from "./worker/app.ui.worker.module";

@NgModule({
  imports: [
    ApplicationUiAnimeModule,
    ApplicationUiWorkerModule,
    ApplicationUiAnimeModule
  ],
})
export class UiModule { }
