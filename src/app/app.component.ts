import { CommonModule } from '@angular/common';
import { Component } from '@angular/core';
import { RouterLink, RouterOutlet } from '@angular/router';
import { UiModule } from './ui/ui.module';
import { ApplicationUiComponentsModule } from './shared/uicomponents/app.uicomponents.module';

@Component({
  selector: 'app-root',
  standalone: true,
  imports:
  [
    CommonModule,
    RouterOutlet,
    RouterLink,
    UiModule,
    ApplicationUiComponentsModule
  ],
  templateUrl: './app.component.html',
  styleUrl: './app.component.scss'
})
export class AppComponent {
  isActiveNavbar = true;
}
