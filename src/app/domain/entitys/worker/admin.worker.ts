import { Worker } from "./worker";

export type AdminWorker = {
  id?: string;
  name: string;
  email: string;
  password: string;
  workers?: Array<Worker>
}
