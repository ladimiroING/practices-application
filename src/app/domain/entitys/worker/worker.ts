import { Departament } from "./departament";

export type Worker = {
  id?: string;
  name: string;
  lastName: string;
  email: string;
  departament: Array<Departament>;
  isActive: boolean;
  img: string;
  adminIdWorker?: string;
  idDepartament?: string;
}
