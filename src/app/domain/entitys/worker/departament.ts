import { Worker } from "./worker";

export type Departament = {
  id?: string;
  area: string;
  workers: Array<Worker>
}
