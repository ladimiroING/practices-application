export type Information = {
  id: string;
  name: string;
  autor: string;
  launch: string;
  gender: string;
  img: string;
}
