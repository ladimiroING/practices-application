import { Observable } from "rxjs";

export interface Crud <T>{
  create(obj: T): Observable<T>;
  read(): Observable<T[]>;
  update(obj: T, id: string): Observable<T>;
  delete(id: string): void;
  showId(id: string): Observable<T>;
}
