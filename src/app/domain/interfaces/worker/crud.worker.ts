import { Worker } from "../../entitys/worker/worker";
import { Crud } from "../crudgeneric/crud";

export interface CrudWorker extends Crud<Worker> { }
