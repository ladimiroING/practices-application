import { AdminWorker } from "../../entitys/worker/admin.worker";
import { Crud } from "../crudgeneric/crud";

export interface CrudAdminWorker extends Crud<AdminWorker> { }
