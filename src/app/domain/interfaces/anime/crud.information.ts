import { Information } from "../../entitys/anime/information";
import { Crud } from "../crudgeneric/crud";

export interface CrudInformation extends Crud<Information> { }
