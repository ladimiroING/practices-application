import { NgModule } from "@angular/core";
import { NavbarComponent } from "./navbar/navbar.component";
import { RouterLink } from "@angular/router";

@NgModule({
  declarations: [
    NavbarComponent
  ],
  imports: [
    RouterLink
  ],
  exports: [
    NavbarComponent
  ]
})
export class ApplicationUiComponentsModule { }
